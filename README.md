# Holly Microservices Prototype

![hollyInsomnia](/uploads/5aeb77c6b1671fd7710ccb5df5e1c75a/hollyInsomnia.jpg)

Summary:

- Roundtrip: Client -> ApiGateway -> Operations MS -> ApiGateway -> Client.
- Z niceho nevychazi, tedy zadny balast. Urcite odlisne od M$ eshop on containers.
- Vytvari vlastni network a assignuje staticke IPs pro jednotlive services.

**How to run**

- Docker musi hostovat win containery (kdyztak v guicku Switch to windows containers... nebo DockerCli.exe -SwitchDaemon).
- Build&run.
- Otevre se http://172.16.238.2/api/testmessage
- Pokud se zblazni jiz otevreny browser (nalepi se to na novou sit) kvuli proxy, staci restartovat browser nebo pouzit nejakeho rest klienta.