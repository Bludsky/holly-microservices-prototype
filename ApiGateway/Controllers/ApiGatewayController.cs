﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiGateway.Client;
using Commons.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiGateway.Controllers
{
    [Produces("application/json")]
    [Route("api")]
    public class ApiGatewayController : Controller
    {

        private readonly IRestClient _restClient = new DefaultRestClient();

        private const String OperationsUrl = "http://172.16.238.3/api/acceptandreturn";

        /// <summary>
        /// Roundtrip: Client ApiGateway#TestMessage -> Microservice -> RabbitMQ -> Microservice -> ApiGateway -> Client
        /// </summary>
        [HttpGet]
        [Route("testmessage")]
        public IActionResult TestMessage()
        {
            var messageDto = new MessageDTO { MessageId = "testId", Payload = "Untouched payload"};
            return Json(_restClient.CallPost<MessageDTO, MessageDTO>(messageDto, OperationsUrl));
        }
    }
}