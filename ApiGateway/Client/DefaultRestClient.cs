﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ApiGateway.Client
{
    public class DefaultRestClient : IRestClient
    {
        public U CallPost<T, U>(T payload, String url)
        {
            // No application/json constant/enum in .net?
            var json = new StringContent(JsonConvert.SerializeObject(payload), Encoding.UTF8, "application/json");
            using (var httpClient = new HttpClient())
            {
                var response = httpClient.PostAsync(url, json).Result;
                response.EnsureSuccessStatusCode();
                return JsonConvert.DeserializeObject<U>(response.Content.ReadAsStringAsync().Result);
            }
        }
    }
}
