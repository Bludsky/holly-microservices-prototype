﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace ApiGateway.Client
{
    interface IRestClient
    {
        U CallPost<T, U>(T payload, String url);
    }
}
