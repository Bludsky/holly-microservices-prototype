﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Commons.Models
{
    public class MessageDTO
    {
        public String MessageId { get; set; }
        public String Payload { get; set; }
    }
}
