﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Commons.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Operations.Controllers
{
    [Produces("application/json")]
    [Route("api")]
    public class OperationsController : Controller
    {

        /// <summary>
        /// 1. Accepts request from ApiGateway
        /// 2. Creates TextMessage
        /// 3. Inserts the message into RabbitMQ queue
        /// 4. Retrieve the message
        /// 5. Send the result back to ApiGateway
        /// </summary>
        [HttpPost]
        [Route("acceptandreturn")]
        public IActionResult AcceptAndReturn([FromBody] MessageDTO messageDto)
        {
            messageDto.Payload = "Touched by operations microservice";
            return Json(messageDto);
        }

    }
}